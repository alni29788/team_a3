# Using library for thingspeak from https://github.com/mchwalisz/thingspeak
import thingspeak
# time is used to introduce delays in the program
import time
from functions import *

channel_id = get_key('channel.txt')  # PUT YOUR CHANNEL ID IN A FILE CALLED channel.txt LOCATED IN THE SAME FOLDER AS main.py
write_key = get_key('write_key.txt') #PUT YOUR API WRITE KEY IN A FILE CALLED wrirte_key.txt LOCATED IN THE SAME FOLDER AS main.py

if __name__ == '__main__':

    # initialize the connection to thingspeak
    channel = thingspeak.Channel(id=channel_id, api_key=write_key)

    # Test by sending some values to thingspeak {channel: value}
    response = channel.update({1: 1})
    # print the response from thingspeak
    print(response)
    # wait 15 seconds because of thingspeak quota limit https://thingspeak.com/pages/license_faq
    time.sleep(15)

    response = channel.update({1: 2})
    print(response)
    time.sleep(15)

    response = channel.update({1: 3})
    print(response)
    time.sleep(15)

    response = channel.update({1: 4})
    print(response)
    time.sleep(15)

    response = channel.update({1: 5})
    print(response)
    time.sleep(15)
