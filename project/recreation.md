IMPORTANT!!! don't forget to install thingspeak and w1thermsensor in pip(or use requirements file) AND
to clone this repository into your pi, so you'd not get problems with env, pycache and our system would be
able to work properly.
Minimum system setup.
We create the system as shown below using the following:
2x Buttons
2x LED +220 Ohm resistors for each of them
1 mcp3008
1DS18B20 +single 10k Ohm resistor for it
1 potentiometer
A lot of wires as required throughout.

![Setup](Set-up.png)

![Picture](img/pic1.png)

![Picture](img/pic2.png)

![Picture](img/pic3.png)

![Picture](img/pic4.png)

![Picture](img/pic5.png)


Code
main.py 
when you turn the potentiometer it will change the voltage and when it is >0.5 then the LED will
flick on/off. The Temperature is printed through the code on your raspberry pi when you run the
program. The only variable is printed on the screen since the button click counter is supposed to
show click counts on the Thingspeak. (even though Thingspeak doesn’t work, if you will change
print command from temperature(can’t leave it, since it updates values too often)to click counts
it will work )
ThingSpeak setup (doesn’t work so ignore it)
First open up the thingspeak website at https://thingspeak.com/channels
Once signed in or account created, click on the ‘New Channel’ button

![Picture][img/channel.png]

Click Save channel
Copy the Channel ID as you will need this for later. Save it as channel.txt and make sure that there is a line
break after the number.
Click on the API write keys tab, copy the key and save it as write_key.txt, with a line break afterwards.
Make sure that both of the files above are in the same folder as the code.
The files created above will be included in the code to reference your channel when you are attempting to
upload the program data to ThingSpeak.

Code

Functions.py 

```import sys
import time # used to get timestamp
import math # used to do math
from thingspeak import Channel # import thingspeak
from gpiozero import PWMLED, Button, MCP3008
from functions import *
from w1thermsensor import W1ThermSensor #library for the sensor https://pypi.org/project/w1thermsensor/

channel_id = get_key('channel.txt')  # PUT YOUR CHANNEL ID IN A FILE CALLED channel.txt LOCATED IN THE SAME FOLDER AS main.py
write_key = get_key('write_key.txt') #PUT YOUR API WRITE KEY IN A FILE CALLED wrirte_key.txt LOCATED IN THE SAME FOLDER AS main.py

# start values
btn1_value=0
btn2_value=0
ADC_value=0
avg_temp=0

# define pin numbers
btn1pin=18
btn2pin=12
led1pin=17
led2pin=25


pot=MCP3008(0)
Led1=PWMLED(led1pin)
Led1.value=0
Led2=PWMLED(led2pin)
Led2.value=0

# define functions
def now_ms():
    ms = int(time.time() * 1000)
    return ms

def readBtn1():
    global btn1_value
	btn1_value=btn1_value+1
    print("Button 1 pressed "+str(btn1_value)+" times")
    
def readBtn2():
    global btn2_value
	btn2_value=btn2_value+1
    print("Button 2 pressed "+str(btn2_value)+" times")
    
btn1=Button(btn1pin)
btn1.when_pressed = readBtn1()
btn2=Button(btn2pin)
btn2.when_pressed = readBtn2()

def readADC():
    value=pot.value
    voltage = value / 255.0 * 3.3  # calculate the voltage value
    if(float(pot.value)>0.5):
        updateLed1()
    return voltage
	
def readTemp():
    global avg_temp
    # initialize the sensor
    sensor = W1ThermSensor()
    temperature_in_celsius = sensor.get_temperature()
    avg_temp=temperature_in_celsius
    print("Temp: "+str(temperature_in_celsius))
        
def updateLed1():
    for x in range(6):
        if(x % 2)==0:
            Led1.value=1
        else:
            Led1.value=0
        time.sleep(0.2)

def updateLed2():
    Led2.value=1
    time.sleep(0.5)
    Led2.value=0

def thingspeakSend(btn1_value, btn2_value, ADC_value, avg_temp):
    try:
        channel = thingspeak.Channel(id=channel_id, api_key=write_key)
        response = channel.update({1: btn1_value, 2: btn2_value, 3: ADC_value, 4: avg_temp})
        updateLed2()
    except:
        # If the connection to thingspeak fails
        print('connection failed')

# initialize variables
loop_duration = 15000 # in milliseconds
loop_start = now_ms()

def loop():
    global loop_start, btn1_value, btn2_value, ADC_value, avg_temp
    while True:
        if (now_ms() - loop_start > loop_duration):
            # send values to Thingspeak here
            print('sending to Thingspeak')
            thingspeakSend(btn1_value, btn2_value, ADC_value, avg_temp)
            btn1_value=0
            btn2_value=0
            ADC_value=0
            avg_temp=0
            loop_start = now_ms()
            continue # continue to top of while loop
        else:
            # read senors, buttons and update led's here
            readBtn1()
            readBtn2()
            ADC_value=readADC()
            readTemp()
            print(f'loop elapsed ms {now_ms() - loop_start}') # print loop time
            

    
if __name__ == '__main__':   # Program entrance
    print ('Program is starting ... ')
    try:
        loop()
    except KeyboardInterrupt: # Press ctrl-c to end the program.
        print('Program is ended')```
        
    