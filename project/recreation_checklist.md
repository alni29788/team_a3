Recreation checklist for team <A4>
===================================================
Team name: <A3>
Checklist
------------
- [-] Project plan completed
- [-] System block diagram completed
- [+] Recreate documentation clear and understandable
- [-] ADC communicating with RPi
- [+] Temperature sensor communicating with RPi
- [+] BTN/LED board communicating with RPi
- [+] Data from RPi sent to Thingspeak
Comments
-----------
No plan and block diagram
ADC error message:
Traceback (most recent call last): File "/home/pi/team_a3/project/main.py", line 195,
in <module> ADC_value[0]=readADC(ADC_value[0]) File
"/home/pi/team_a3/project/main.py", line 66, in readADC value =
adc[0].analogRead(0) # read the ADC value of channel 0 File
"/usr/local/lib/python3.7/dist-packages/ADCDevice-1.0.3-
py3.7.egg/ADCDevice/ADCDevice.py", line 51, in analogRead value =
self.bus.read_byte_data(self.address, self.cmd|(((chn<<2 | chn>>1)&0x07)<<4))
OSError: [Errno 121] Remote I/O error
Recreation checklist for team <B4>
===================================================
Team name: <A3>
Checklist
------------
- [-] Project plan completed
- [-] System block diagram completed
- [-] Recreate documentation clear and understandable
- [-] ADC communicating with RPi
- [-] Temperature sensor communicating with RPi
- [-] BTN/LED board communicating with RPi
- [-] Data from RPi sent to Thingspeak
Comments
-----------
The only things B4 has are the links to the guides. There’s nothing
about the minimum systems.