Contact Information


Mark: 
https://www.facebook.com/mark.christiansens
https://www.linkedin.com/in/mark-christiansen-b99907139
google mail: zinexx@gmail.com 
personal mail: markic15@hotmail.com

Robert:
https://www.facebook.com/bogdan.robert.7370
bogdan.rrobert@gmail.com 

Gabriel:
 https://www.facebook.com/cangabriel.bekil
bekilcangabriel@gmail.com

Jacob:
https://www.facebook.com/jacob.koch.7792
Google email: bergkoch@yahoo.dk
Personal email: jacobkoch94@yahoo.dk 

Andre:
https://www.facebook.com/AndreRomar/
andrei.romar@gmail.com

Kevin:
https://www.facebook.com/KevinHerholdLarsen
Kevinhlarsen@gmail.com

Discord group link: 
https://discord.gg/nSXFGQ

IT Technology (Class A) (Class facebook)


_______________________________________

Team contract:

Each group member agrees to show up to the group meetings on time. It is okay to cancel in advance. Cancellations are expected due to illness (needs to be told in the morning hours before meeting). If you are not able to come to the meeting, you have to give a justifiable reason in time, e.g. work, sickness, other personal reasons. If a group member skips meetings then he gets a warning. Upon achieving a second warning, an inside-group meeting is held. If no changes occur and 3rd warning is awarded then the group asks for help from the above(teacher/coordinator/employer).

In some cases, it is okay for the group to make decisions by majority. If there are simple disagreements the majority will decide. Compromise can also be made if it is, for example, when to meet.

At the beginning of a work, expectations and level of ambition are agreed. Agreed expectations must be met. Everyone must live up to expectations. It is up to the individual whether they will put extra effort into the work. Expectations are based on effort, not the amount of results. Expectations and level of ambition can change if the work shows that the task has a different scope than anticipated.
Work accordingly to the GanttChart(or any other planning function) if we fall behind on the schedule the group will need to present a realistic catch up plan for the work needed


The members of the group are responsible for their own benefit from the cooperation. It is still expected that all team members are satisfied with the outcome of the work.


There should be room to talk about collaboration and people's approach to group work as long as the intention is to improve the group work. In addition, a nice tone is expected for all group members.


If the individual member needs to talk about something private. This will be done in the beginning of the meeting so that the rest of the group can take it into account. Other emotional things can be talked about during breaks.


The meetings include breaks where personal matters can be discussed or coffee breaks are held. The breaks are determined jointly during the meeting so that the work is still efficient.


If a member of the group does not show up to the meeting most of the time or does not bring the expected results and ambition, then an ‘emergency meeting’ will be held to talk about those problems. If the problems can not be resolved, then the group will come to a decision on what the next steps regarding that issue are going to be. 
